import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

import { BrowserModule } from '@angular/platform-browser';

import { AboutComponent } from './features/about/about.component';
import { HomeComponent } from './features/home/home.component';
import { TopArtistsComponent } from './features/top-artists/top-artists.component';
import { TopTracksComponent } from './features/top-tracks/top-tracks.component';
import { OverviewComponent } from './features/overview/overview.component';
import { AuthComponent } from './features/auth/auth.component';
import { DiscoverComponent } from './features/discover/discover.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'auth', component: AuthComponent },
  { 
    path: 'user',
    canActivate: [AuthGuard],
    children: [
      { path: 'overview', component: OverviewComponent },
      { path: 'discover', component: DiscoverComponent },
      { path: 'top-tracks', component: TopTracksComponent },
      { path: 'top-artists', component: TopArtistsComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes), 
    BrowserModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
