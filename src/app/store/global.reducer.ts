import { initialGlobalState, GlobalState } from './global.state';

import { GlobalActions, GlobalActionTypes } from './global.actions';

const newState = ( state, newData ) => {
  return Object.assign({}, state, newData );
};

export function globalReducers (
  state: GlobalState = initialGlobalState,
  action: GlobalActions
) {
  switch(action.type) {
    case GlobalActionTypes.GetTopArtistsSuccessAction: {
      return newState(state, {
        topArtists: action.payload.topArtists,
        topGenres: action.payload.topGenres
      });
    }

    case GlobalActionTypes.GetTopTracksSuccessAction: {
      let tracks = action.payload.topTracks;

      tracks.longTerm.map(track => track.nowPlaying = false);
      tracks.mediumTerm.map(track => track.nowPlaying = false);
      tracks.shortTerm.map(track => track.nowPlaying = false);
      
      return newState(state, {
        topTracks: tracks,
        isNewUser: action.payload.topTracks.longTerm.length <= 5
      });
    }

    case GlobalActionTypes.GetUserDetailsSuccessAction: {
      return newState(state, {
        userDetails: action.payload.userDetails
      });
    }

    default: 
      return state;
  }
}
