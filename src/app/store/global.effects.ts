import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { AppService } from '../services/app.service';
import { 
  GlobalActionTypes, 
  GetTopArtistsSuccessAction, GetTopArtistsErrorAction, 
  GetTopTracksSuccessACtion, GetTopTracksErrorAction, 
  GetUserDetailsSuccessAction, GetUserDetailsErrorAction, GetTopArtistsAction, GetTopTracksAction, GetUserDetailsAction } 
  from './global.actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { AppState } from './app.state';

@Injectable()
export class GlobalEffects {

  constructor(
    private actions$: Actions,
    private appService: AppService,
    private authService: AuthService,
    private appStore: Store<AppState>
  ) {}
  
  @Effect()
  getTopArtists$: Observable<Action> = this.actions$
  .pipe(
    ofType(GlobalActionTypes.GetTopArtistsAction),
    switchMap(() => {
      return this.appService.getTopArtists$().pipe(
        map((data) => new GetTopArtistsSuccessAction({ topArtists: data.artists, topGenres: data.genres })),
        catchError((error) => of(new GetTopArtistsErrorAction({ error: error })))
      );
  }));

  @Effect({dispatch: false})
  getTopArtistsError = this.actions$
    .pipe(
      ofType(GlobalActionTypes.GetTopArtistsErrorAction),
      map((action: GetTopArtistsErrorAction) => {
        if (action.payload.error.status === 401) {
          this.authService.requestRefreshToken$().subscribe(value => {
            this.authService.setAccessToken(value.access_token);
            
            this.appStore.dispatch(new GetTopArtistsAction());
            this.appStore.dispatch(new GetTopTracksAction());
            this.appStore.dispatch(new GetUserDetailsAction());
          });
        }
      }));

  @Effect()
  getTopTracks$: Observable<Action> = this.actions$
  .pipe(
    ofType(GlobalActionTypes.GetTopTracksAction),
    switchMap(() => {
      return this.appService.getTopTracks$().pipe(
        map((data) => new GetTopTracksSuccessACtion({ topTracks: data })),
        catchError((error) => of(new GetTopTracksErrorAction({ error: error })))
      );
  }));

  @Effect()
  getUserDetails$: Observable<Action> = this.actions$
  .pipe(
    ofType(GlobalActionTypes.GetUserDetailsAction),
    switchMap(() => {
      return this.appService.getUserDetails$().pipe(
        map((data) => new GetUserDetailsSuccessAction({ userDetails: data })),
        catchError((error) => of(new GetUserDetailsErrorAction({ error: error })))
      );
  }));

}