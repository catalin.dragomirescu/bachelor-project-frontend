import { GlobalState } from './global.state';

export interface AppState {
  global: GlobalState;
}
