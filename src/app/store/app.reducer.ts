import { ActionReducerMap, ActionReducer, MetaReducer } from '@ngrx/store';
import { storeLogger } from 'ngrx-store-logger';

import { AppState } from './app.state';
import { globalReducers } from './global.reducer';
import { environment } from 'src/environments/environment';

export const appReducers: ActionReducerMap<AppState> = {
  global: globalReducers
};

export function logger(reducer: ActionReducer<AppState>): any {
  return storeLogger({
    collapsed: true,
    /*duration: false,*/
    timestamp: false,
    colors: {
      title: () => '#455A64',
      prevState: () => '#795548',
      nextState: () => '#4CAF50',
      action: () => '#2196F3',
      error: () => '#f44336'
    }
  })(reducer);
}

export const metaReducers: MetaReducer<AppState>[] = environment.production ? [] : [logger];
