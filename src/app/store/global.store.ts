import { createSelector, createFeatureSelector } from '@ngrx/store';
import { GlobalState } from './global.state';

export class GlobalStateProps {
  // public static readonly recommendations = (state: GlobalState) => state.recommendations;
  public static readonly topArtists = (state: GlobalState) => state.topArtists;
  public static readonly topTracks = (state: GlobalState) => state.topTracks;
  public static readonly topGenres = (state: GlobalState) => state.topGenres;
  public static readonly userDetails = (state: GlobalState) => state.userDetails;
  public static readonly isNewUser = (state: GlobalState) => state.isNewUser;
}

export const globalState = createFeatureSelector<GlobalState>('global')

export class GlobalStoreProps {
  // public static readonly recommendations = createSelector(globalState, GlobalStateProps.recommendations);
  public static readonly topArtists = createSelector(globalState, GlobalStateProps.topArtists);
  public static readonly topTracks = createSelector(globalState, GlobalStateProps.topTracks);
  public static readonly topGenres = createSelector(globalState, GlobalStateProps.topGenres);
  public static readonly userDetails = createSelector(globalState, GlobalStateProps.userDetails);
  public static readonly isNewUser = createSelector(globalState, GlobalStateProps.isNewUser);
}
