import { Action } from '@ngrx/store';

export enum GlobalActionTypes {
  GetTopArtistsAction = 'Get Top Artists',
  GetTopArtistsSuccessAction = 'Get Top Artists Success',
  GetTopArtistsErrorAction = 'Get Top Artists Error',
  GetTopTracksAction = 'Get Top Tracks',
  GetTopTracksSuccessAction = 'Get Top Tracks Success',
  GetTopTracksErrorAction = 'Get Top Tracks Error',
  GetUserDetailsAction = 'Get User Details',
  GetUserDetailsSuccessAction = 'Get User Details Success',
  GetUserDetailsErrorAction = 'Get User Details Error'
}

export class GetTopArtistsAction implements Action {
  readonly type = GlobalActionTypes.GetTopArtistsAction;
  constructor() { }
}

export class GetTopArtistsSuccessAction implements Action {
  readonly type = GlobalActionTypes.GetTopArtistsSuccessAction;
  constructor(public payload: { topArtists: any, topGenres: any }) { }
}

export class GetTopArtistsErrorAction implements Action {
  readonly type = GlobalActionTypes.GetTopArtistsErrorAction;
  constructor(public payload: { error: any }) { }
}

export class GetTopTracksAction implements Action {
  readonly type = GlobalActionTypes.GetTopTracksAction;
  constructor() { }
}

export class GetTopTracksSuccessACtion implements Action {
  readonly type = GlobalActionTypes.GetTopTracksSuccessAction;
  constructor(public payload: { topTracks: any }) { }
}

export class GetTopTracksErrorAction implements Action {
  readonly type = GlobalActionTypes.GetTopTracksErrorAction;
  constructor(public payload: { error: any }) { }
}

export class GetUserDetailsAction implements Action {
  readonly type = GlobalActionTypes.GetUserDetailsAction;
  constructor() { }
}

export class GetUserDetailsSuccessAction implements Action {
  readonly type = GlobalActionTypes.GetUserDetailsSuccessAction;
  constructor(public payload: { userDetails: any }) { }
}

export class GetUserDetailsErrorAction implements Action {
  readonly type = GlobalActionTypes.GetUserDetailsErrorAction;
  constructor(public payload: { error: any }) { }
}

export type GlobalActions = 
GetTopArtistsAction |
GetTopArtistsSuccessAction |
GetTopArtistsErrorAction |
GetTopTracksAction |
GetTopTracksSuccessACtion |
GetTopTracksErrorAction |
GetUserDetailsAction |
GetUserDetailsSuccessAction |
GetUserDetailsErrorAction
;
