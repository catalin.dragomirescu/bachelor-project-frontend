export interface GlobalState {
    userDetails: any;
    topArtists: any;
    topTracks: any;
    topGenres: any;
    isNewUser: boolean;
}

export const initialGlobalState: GlobalState = {
    userDetails: null,
    topArtists: [],
    topTracks: [],
    topGenres: [],
    isNewUser: null
}
