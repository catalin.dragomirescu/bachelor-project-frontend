import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { tap, map, catchError } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  topArtists: any[];
  topTracks: any[];
  userDetails: any;

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  getTopArtists$(): Observable<any> {
    return this.http.get<any>(environment.apiUrl + 'api/user-top-artists', this.getHttpOptions());
  }

  getTopTracks$(): Observable<any> {
    return this.http.get<any>(environment.apiUrl + 'api/user-top-tracks', this.getHttpOptions());
  }

  getUserDetails$(): Observable<any> {
    return this.http.get<any>(environment.apiUrl + 'api/user-details', this.getHttpOptions());
  }

  getRecommendations$(payload: any): Observable<any> {
    return this.http.post<any>(environment.apiUrl + 'api/recommendations', payload, this.getHttpOptions());    
  }

  getTracksBasedRecommendations$(payload: any): Observable<any> {
    return this.http.post<any>(environment.apiUrl + 'api/tracks-based-recommendations', payload, this.getHttpOptions());    
  }

  getManualFiltersRecommendations$(payload: any): Observable<any> {
    return this.http.post<any>(environment.apiUrl + 'api/manual-filters-recommendations', payload, this.getHttpOptions());    
  }

  createPlaylist$(userId: string, playlistName: string): Observable<any> {
    return this.http.get<any>(environment.apiUrl + 'api/create-playlist?user_id=' + userId + '&playlist_name=' + playlistName, this.getHttpOptions());
  }

  addTracksToPlaylist$(trackListPayload: any): Observable<any> {
    return this.http.post<any>(environment.apiUrl + 'api/add-tracks-to-playlist', trackListPayload, this.getHttpOptions());
  }

  searchArtist$(artistQuery: string): Observable<any> {
    return this.http.get<any>(environment.apiUrl + 'api/search?search_query=' + artistQuery, this.getHttpOptions());
  }

  getGenres$(): Observable<any> {
    return this.http.get<any>(environment.apiUrl + 'api/get-genres', this.getHttpOptions());
  }

  private getHttpOptions() {
    return {
      headers: new HttpHeaders({
        'Authorization': this.authService.getAccessToken()
      })
    };
  }

}
