import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAuthenticated: boolean = false;

  constructor(private http: HttpClient) { }

  public setAccessToken(accessToken: string): void {
    window.sessionStorage.setItem('accessToken', accessToken);
  }

  public setRefreshToken(refreshToken: string): void {
    window.sessionStorage.setItem('refreshToken', refreshToken);
  }

  public getAccessToken(): string {
    return window.sessionStorage.getItem('accessToken');
  }
  
  public getRefreshToken(): string {
    return window.sessionStorage.getItem('refreshToken');
  }

  public isLoggedIn(): boolean {
    let accessToken: string = window.sessionStorage.getItem('accessToken');
    let refreshToken: string = window.sessionStorage.getItem('refreshToken');

    return accessToken !== null && accessToken !== undefined && refreshToken !== null && refreshToken !== undefined;
  }

  public logout(): void {
    this.isAuthenticated = false;
    
    window.location.href = '/'
    window.sessionStorage.removeItem('accessToken');
    window.sessionStorage.removeItem('refreshToken');
  }

  requestRefreshToken$(): Observable<any> {
    return this.http.get<any>(environment.apiUrl + 'refresh_token?refresh_token=' + this.getRefreshToken());
  }
}
