import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Store } from '@ngrx/store';
import { GlobalState } from '../store/global.state';
import { GetTopArtistsAction, GetTopTracksAction, GetUserDetailsAction } from '../store/global.actions';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private appStore: Store<GlobalState> 
  ) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkAuthentication();
  }

  checkAuthentication(): boolean {
    if (this.authService.isLoggedIn()) {
      this.getUserData();
      return true;
    }

    // Navigate to home page if the user is not authenticated
    this.router.navigate(['/']);
    return false;
  }

  private getUserData() {
    this.appStore.dispatch(new GetTopArtistsAction());
    this.appStore.dispatch(new GetTopTracksAction());
    this.appStore.dispatch(new GetUserDetailsAction());
  }
}
