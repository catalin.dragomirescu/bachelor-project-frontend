export class TrackModel {
  title: string;
  artist: string;

  constructor(value: Object = {}) {
    Object.assign(this, value);
  }
}