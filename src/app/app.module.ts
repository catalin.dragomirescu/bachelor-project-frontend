import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';

import { AppService } from './services/app.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './features/home/home.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { AboutComponent } from './features/about/about.component';
import { TopTracksComponent } from './features/top-tracks/top-tracks.component';
import { TopArtistsComponent } from './features/top-artists/top-artists.component';
import { OverviewComponent } from './features/overview/overview.component';
import { AuthComponent } from './features/auth/auth.component';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { GlobalEffects } from './store/global.effects';
import { appReducers, metaReducers } from './store/app.reducer';
import { SidenavComponent } from './components/side-nav/side-nav.component';
import { DiscoverComponent } from './features/discover/discover.component';
import { MobileToolbarComponent } from './components/mobile-toolbar/mobile-toolbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    AboutComponent,
    TopTracksComponent,
    TopArtistsComponent,
    OverviewComponent,
    AuthComponent,
    SidenavComponent,
    DiscoverComponent,
    MobileToolbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialModule,
    HttpClientModule,

    StoreModule.forRoot( appReducers, { metaReducers }),
    EffectsModule.forRoot([ GlobalEffects ])
  ],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
