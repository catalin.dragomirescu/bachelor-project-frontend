import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isMobile: boolean;

  constructor(public router: Router) {}

  ngOnInit() {
    this.isMobile = this.isMobileDevice();
  }

  isMobileDevice() {
    return window.innerWidth <= 700;
  }

  onResize(event) {
    this.isMobile = event.target.innerWidth <= 700
  }
}
