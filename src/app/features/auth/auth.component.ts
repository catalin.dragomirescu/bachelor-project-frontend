import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AppService } from 'src/app/services/app.service';
import { Store } from '@ngrx/store';
import { GlobalState } from 'src/app/store/global.state';
import { GetTopArtistsAction, GetTopTracksAction, GetUserDetailsAction } from 'src/app/store/global.actions';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(
    private appService: AppService,
    private authService: AuthService,
    private appStore: Store<GlobalState>,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    const isAuthParam: string = this.route.snapshot.queryParamMap.get('is_authenticated');

    if (isAuthParam) {
      this.authService.setAccessToken(this.route.snapshot.queryParamMap.get('access_token'));
      this.authService.setRefreshToken(this.route.snapshot.queryParamMap.get('refresh_token'));
      this.authService.isAuthenticated = true;

      
      this.router.navigate(['/user/overview']);
    } else {
      this.router.navigate(['/']);
    }
  }
}
