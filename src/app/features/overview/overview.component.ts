import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { GlobalState } from 'src/app/store/global.state';
import { GlobalStoreProps } from 'src/app/store/global.store';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  userDetails: any;

  topArtists: any[];
  topTracks: any[];
  topGenres: any[];

  breakpoint: number;

  public topArtistsSubscriber: Subscription;
  public topTracksSubscriber: Subscription;
  public topGenresSubscriber: Subscription;
  public userDetailsSubscriber: Subscription;
  newUserSubscriber: Subscription;
  isNewUser: boolean = true;
  
  constructor(
    private appStore: Store<GlobalState>) { }

  ngOnInit() {
    window.scroll(0,0);


    this.breakpoint = window.innerWidth <= 850 ? 2 : 4;

    this.topArtistsSubscriber = this.appStore.select(GlobalStoreProps.topArtists).subscribe((artists) => {
      if (artists) {
        this.topArtists = artists;
      }
    });

    this.topTracksSubscriber = this.appStore.select(GlobalStoreProps.topTracks).subscribe((tracks) => {
      if (tracks) {
        this.topTracks = tracks;
      }
    });

    this.topGenresSubscriber = this.appStore.select(GlobalStoreProps.topGenres).subscribe((genres) => {
      if (genres) {
        this.topGenres = genres;
      }
    });

    this.userDetailsSubscriber = this.appStore.select(GlobalStoreProps.userDetails).subscribe((userDetails) => {
      if (userDetails) {
        this.userDetails = userDetails;
      }
    });

    this.newUserSubscriber = this.appStore.select(GlobalStoreProps.isNewUser).subscribe((isNewUser: boolean) => {
      this.isNewUser = isNewUser;
    });
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 850) ? 2 : 4;
  }

  ngOnDestroy() {
    if (this.topArtistsSubscriber) {
      this.topArtistsSubscriber.unsubscribe();
    }
    if (this.topTracksSubscriber) {
      this.topTracksSubscriber.unsubscribe();
    }
    if (this.topGenresSubscriber) {
      this.topGenresSubscriber.unsubscribe();
    }
    if (this.userDetailsSubscriber) {
      this.userDetailsSubscriber.unsubscribe();
    }
    if (this.newUserSubscriber) {
      this.newUserSubscriber.unsubscribe();
    }
  }

}
