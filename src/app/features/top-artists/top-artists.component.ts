import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/services/app.service';
import { AppState } from 'src/app/store/app.state';
import { Store } from '@ngrx/store';
import { GlobalStoreProps } from 'src/app/store/global.store';
import { Subscription } from 'rxjs';
import { GlobalState } from 'src/app/store/global.state';

@Component({
  selector: 'app-top-artists',
  templateUrl: './top-artists.component.html',
  styleUrls: ['./top-artists.component.scss']
})
export class TopArtistsComponent implements OnInit {

  artists: any;
  topArtistsSubscriber: Subscription;
  artistList: any;
  timeRange: string;

  constructor(private appStore: Store<GlobalState>) { }

  ngOnInit() {
    window.scroll(0,0);
    this.topArtistsSubscriber = this.appStore.select(GlobalStoreProps.topArtists).subscribe((artists) => {
      if (artists) {
        this.artists = artists;
        this.artistList = artists.shortTerm;
        this.timeRange = 'shortTerm';
      }
    });
  }

  onTimeRange(timeRange: string) {
    this.timeRange = timeRange;
    switch (timeRange) {
      case 'shortTerm': {
        this.artistList = this.artists.shortTerm;
        break;
      }
      case 'mediumTerm': {
        this.artistList = this.artists.mediumTerm;
        break;
      }
      case 'longTerm': {
        this.artistList = this.artists.longTerm;
        break;
      }
      default: {
        break;
      }
    }
  }

  ngOnDestroy() {
    if (this.topArtistsSubscriber) {
      this.topArtistsSubscriber.unsubscribe();
    }
  }

}
