import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/services/app.service';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { GlobalState } from 'src/app/store/global.state';
import { GlobalStoreProps } from 'src/app/store/global.store';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss']
})
export class DiscoverComponent implements OnInit {

  topArtists: any[];
  topTracks: any;

  recommendationList: any;
  checkList: boolean[];
  userId: string;

  showList: boolean;

  isLinear = true;

  topArtistsSubscriber: Subscription;
  topTracksSubscriber: Subscription;
  userDetailsSubscriber: Subscription;
  searchSubscriber: Subscription;
  genresSubscriber: Subscription;
  newUserSubscriber: Subscription;

  items: any;

  autoFilter: boolean;
  filters: any;
  filterCheckboxes: any;

  searchedArtist: string;
  filteredArtistList: any[] = [];
  selectedArtist: any;

  selectedGenre: string;
  filteredGenreList: string[] = [];
  genreList: string[];

  seedType: string;

  breakpoint: number;

  isNewUser: boolean;

  audio = new Audio();
  currentlyPlaying: any;
  previouslyPlayed: any;

  constructor(
    private appStore: Store<GlobalState>,
    private appService: AppService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    window.scroll(0,0);
    this.breakpoint = (window.innerWidth <= 500) ? 1 : 2;

    this.showList = false;

    this.seedType = "top-artists";

    this.autoFilter = true;
    this.filters = {
      danceability: 0,
      acousticness: 0,
      energy: 0,
      happiness: 0,
      popularity: 0
    }

    this.filterCheckboxes = {
      danceability: false,
      acousticness: false,
      energy: false,
      happiness: false,
      popularity: false
    }

    this.topArtistsSubscriber = this.appStore.select(GlobalStoreProps.topArtists).subscribe((artists) => {
      if (artists) {
        this.topArtists = artists.shortTerm;
      }
    });

    this.topTracksSubscriber = this.appStore.select(GlobalStoreProps.topTracks).subscribe((tracks) => {
      if (tracks) {
        this.topTracks = tracks;
      }
    });

    this.userDetailsSubscriber = this.appStore.select(GlobalStoreProps.userDetails).subscribe((userDetails) => {
      if (userDetails) {
        this.userId = userDetails.id;
      }
    });

    this.newUserSubscriber = this.appStore.select(GlobalStoreProps.isNewUser).subscribe((isNewUser: boolean) => {
      this.isNewUser = isNewUser;
      this.autoFilter = false;
    });

    this.genresSubscriber = this.appService.getGenres$().subscribe(genreList => {
      this.genreList = genreList.genres;
    })
  }

  onArtistSearchChange(searchValue : string) {
    if (searchValue.length >= 2) {
      this.searchSubscriber = this.appService.searchArtist$(searchValue).subscribe((value) => {
        this.filteredArtistList = value.artists.items;
      });
    } else {
      this.filteredArtistList = [];
    }
  }

  onGenreSearchChange() {
    if (this.selectedGenre && this.genreList && this.genreList.length > 0) {
      this.filteredGenreList = this.genreList.filter((genre: string) => genre.includes(this.selectedGenre.toLowerCase()));
    }
  }

  displayArtistName(artist: any): string {
    if (artist) {
      this.selectedArtist = artist;
      return artist.name;
    } else {
      return undefined;
    }
  }

  getRecommendations() {
    this.recommendationList = [];
    this.checkList = [];

    this.autoFilter ? this.getAutoFilterRecommendations() : this.getManualFilterRecommendations();
  }

  getAutoFilterRecommendations() {
    let trackIdList = this.topTracks.shortTerm.map(track => track.id).join(',');
    let seedArtists = this.topArtists.slice(0,5).map(artist => artist.id).join(',');

    let payload = {
      trackIdList: trackIdList,
      seedArtists: seedArtists
    };
    this.appService.getTracksBasedRecommendations$(payload).subscribe(recommendations => {
      if (recommendations) {
        this.showList = true;
        this.recommendationList = recommendations;
        this.recommendationList.tracks.forEach(() => this.checkList.push(true));
      }
    });
  }

  getManualFilterRecommendations() {
    let seedArtists = 'none';
    let seedGenre = 'none';

    switch (this.seedType) {
      case 'top-artists': {
        seedArtists = this.topArtists.slice(0,5).map(artist => artist.id).join(',');
        break;
      }
      case 'artist-seed': {
        seedArtists = this.selectedArtist.id;
        break;
      }
      case 'genre-seed': {
        seedGenre = this.selectedGenre;
        break;
      }
      default: {
        break;
      }
    }

    this.filters = {
      danceability: this.filterCheckboxes.danceability ? this.filters.danceability : -1,
      acousticness: this.filterCheckboxes.acousticness ? this.filters.acousticness : -1,
      energy: this.filterCheckboxes.energy ? this.filters.energy : -1,
      happiness: this.filterCheckboxes.happiness ? this.filters.happiness : -1,
      popularity: this.filterCheckboxes.popularity ? this.filters.popularity : -1
    }

    let payload = {
      seedArtists: seedArtists,
      seedGenre: seedGenre,
      filters: this.filters
    };

    this.appService.getManualFiltersRecommendations$(payload).subscribe(recommendations => {
      if (recommendations) {
        this.showList = true;
        this.recommendationList = recommendations;
        this.recommendationList.tracks.forEach(() => this.checkList.push(true));
      }
    });
  }

  createPlaylist() {
    const now = new Date();
    const today: string = now.getDate() + '/' + (now.getMonth() + 1) + '/' + now.getFullYear();

    const playlistName = "Discover Music - Recommendations " + today;

    this.appService.createPlaylist$(this.userId, playlistName).subscribe(playlist => {
      if (playlist) {
        let payload = this.mapToTracksToPlaylistPayload(playlist.id);
        this.appService.addTracksToPlaylist$(payload).subscribe(data => {
          this.snackBar.open('Playlist successfully saved', '', {
            duration: 2000,
          });
        })
      }
    })
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 500) ? 1 : 2;
  }

  mapToTracksToPlaylistPayload(playlistId: string) {
    return {
      playlistId: playlistId,
      tracksUris: this.mapToTrackUris()
    }
  }

  play(selectedTrack: any) {
    this.currentlyPlaying = selectedTrack;

    this.recommendationList.tracks.find(track => track.id === selectedTrack.id).playingNow = true;

    if (this.previouslyPlayed !== selectedTrack) {
      if (this.previouslyPlayed) {
        let lastTrack = this.recommendationList.tracks.find(track => track.id === this.previouslyPlayed.id);

        if (lastTrack) {
          this.recommendationList.tracks.find(track => track.id === this.previouslyPlayed.id).playingNow = false;
        }
      }
      this.audio.src = selectedTrack.preview_url;
      this.audio.load();
    }

    this.audio.play();
    this.previouslyPlayed = selectedTrack;

    this.audio.onended = () => {
      this.recommendationList.tracks.find(track => track.id === this.currentlyPlaying.id).playingNow = false;
    }
  }

  pause(selectedTrack: any) {
    this.recommendationList.tracks.find(track => track.id === selectedTrack.id).playingNow = false;

    this.audio.pause();
  }

  private mapToTrackUris() {
    let trackUris: any[] = this.recommendationList.tracks.map(track => track.uri);
    
    return trackUris.filter((trackUri, i) => this.checkList[i]);
  }

  ngOnDestroy() {
    if (this.topArtistsSubscriber) {
      this.topArtistsSubscriber.unsubscribe();
    }
    if (this.topTracksSubscriber) {
      this.topTracksSubscriber.unsubscribe();
    }
    if (this.userDetailsSubscriber) {
      this.userDetailsSubscriber.unsubscribe();
    }
    if (this.searchSubscriber) {
      this.searchSubscriber.unsubscribe();
    }
    if (this.genresSubscriber) {
      this.genresSubscriber.unsubscribe();
    }
    if (this.newUserSubscriber) {
      this.newUserSubscriber.unsubscribe();
    }

    this.audio.pause();
  }

}
