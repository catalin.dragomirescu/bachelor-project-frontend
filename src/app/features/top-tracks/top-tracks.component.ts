import { Component, OnInit } from '@angular/core';

import { TrackModel } from 'src/app/shared/models/track.model';
import { AppService } from 'src/app/services/app.service';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { GlobalState } from 'src/app/store/global.state';
import { GlobalStoreProps } from 'src/app/store/global.store';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-top-tracks',
  templateUrl: './top-tracks.component.html',
  styleUrls: ['./top-tracks.component.scss']
})
export class TopTracksComponent implements OnInit {

  tracks: any;
  topTracksSubscriber: Subscription;
  userDetailsSubscriber: Subscription;
  userId: any;
  trackList: any;
  timeRange: string;

  audio = new Audio();
  currentlyPlaying: any;
  previouslyPlayed: any;

  constructor(
    private appStore: Store<GlobalState>,
    private appService: AppService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    window.scroll(0,0);
    this.topTracksSubscriber = this.appStore.select(GlobalStoreProps.topTracks).subscribe((tracks) => {
      if (tracks) {
        this.tracks = tracks;
        this.trackList = tracks.shortTerm;
        this.timeRange = 'shortTerm';
      }
    });

    this.userDetailsSubscriber = this.appStore.select(GlobalStoreProps.userDetails).subscribe((userDetails) => {
      if (userDetails) {
        this.userId = userDetails.id;
      }
    });
  }

  onTimeRange(timeRange: string) {
    this.timeRange = timeRange;
    switch (timeRange) {
      case 'shortTerm': {
        this.trackList = this.tracks.shortTerm;
        break;
      }
      case 'mediumTerm': {
        this.trackList = this.tracks.mediumTerm;
        break;
      }
      case 'longTerm': {
        this.trackList = this.tracks.longTerm;
        break;
      }
      default: {
        break;
      }
    }
  }

  saveTopTracksPlaylist() {
    const playlistName = 'Discover Music - Top Tracks'
    this.appService.createPlaylist$(this.userId, playlistName).subscribe(playlist => {
      if (playlist) {
        let payload = this.mapToTracksToPlaylistPayload(playlist.id);
        this.appService.addTracksToPlaylist$(payload).subscribe(data => {
          this.snackBar.open('Playlist successfully saved', '', {
            duration: 2000,
          });
        })
      }
    })
  }

  mapToTracksToPlaylistPayload(playlistId: string) {
    return {
      playlistId: playlistId,
      tracksUris: this.mapToTrackUris()
    }
  }
  
  play(selectedTrack: any) {
    this.currentlyPlaying = selectedTrack;

    this.trackList.find(track => track.id === selectedTrack.id).playingNow = true;

    if (this.previouslyPlayed !== selectedTrack) {
      if (this.previouslyPlayed) {
        let lastTrack = this.trackList.find(track => track.id === this.previouslyPlayed.id);

        if (lastTrack) {
          this.trackList.find(track => track.id === this.previouslyPlayed.id).playingNow = false;
        }
      }
      this.audio.src = selectedTrack.preview_url;
      this.audio.load();
    }

    this.audio.play();
    this.previouslyPlayed = selectedTrack;

    this.audio.onended = () => {
      this.trackList.find(track => track.id === this.currentlyPlaying.id).playingNow = false;
    }
  }

  pause(selectedTrack: any) {
    this.trackList.find(track => track.id === selectedTrack.id).playingNow = false;

    this.audio.pause();
  }

  private mapToTrackUris() {
    return this.trackList.map(track => track.uri);
  }

  ngOnDestroy() {
    if (this.topTracksSubscriber) {
      this.topTracksSubscriber.unsubscribe();
    }

    this.audio.pause();
  }

}
